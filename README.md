CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Usage

INTRODUCTION
------------

The Private Message Invite module extends the Private Message module 
and allows thread managers to invite people into their Private Message Thread.

The invited user can choose whether to accept or decline the invitation.
Upon acceptance, the user will add the private message thread.


REQUIREMENTS
------------

This module requires the following modules:

* Private Message (https://www.drupal.org/project/private_message)

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) 
  for further information.


Configuration
------------

* Go to admin > configurations.
* Find the menu with name of "Invite Email Configurations".
* Here you will be able to update invitation email's content.


Usage
-------------

    1. Go to single private message thread box.
    2. Find the "invite member" button and click on it.
    3. Enter the email of authenticated or guest user to invite in your thread.
    4. After that user will recieve the notifcation. 
    5. User will able to accept or reject the invitation.
