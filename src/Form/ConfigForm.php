<?php

namespace Drupal\private_message_invite\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the configuration form for the private message module.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'private_message_invite_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultConfiguration() {
    $config = \Drupal::config('private_message_invite.settings');
    $body_message = 'Hi there!' . "\n\n";
    $body_message .= '[current-user:name] has invited you to become a member of the message thread on [site:name].' . "\n";
    $body_message .= 'If you wish to accept the invitation, you need to create an account first.' . "\n\n";
    $body_message .= 'Please visit the following address in order to do so: [private_message_invite:register_link]' . "\n";
    $body_message .= 'Kind regards,' . "\n";
    $body_message .= 'The [site:name] team';

    $body_message_existing_user = 'Hi there!' . "\n\n";
    $body_message_existing_user .= '[current-user:name] has invited you to become a member of the message thread on [site:name].' . "\n";
    $body_message_existing_user .= 'If you wish to accept the invitation, go to My invitations tab in user profile.' . "\n\n";
    $body_message_existing_user .= 'Please visit the following address in order to do so: [private_message_invite:member_invitations_link]' . "\n";
    $body_message_existing_user .= 'Kind regards,' . "\n";
    $body_message_existing_user .= 'The [site:name] team';

    return [
      'invitation_anonymous_subject' => $config->get('invitation_anonymous_subject') ? $config->get('invitation_anonymous_subject') : 'You have a pending Message Thread invitation',
      'invitation_anonymous_body' => $config->get('invitation_anonymous_body') ? $config->get('invitation_anonymous_body') : $body_message,
      'existing_user_invitation_subject' => $config->get('existing_user_invitation_subject') ? $config->get('existing_user_invitation_subject') : 'You have a pending Message Thread invitation',
      'existing_user_invitation_body' => $config->get('existing_user_invitation_body') ? $config->get('existing_user_invitation_body') : $body_message_existing_user,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'private_message_invite.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $formState) {
    $form['invitation_config_tabs'] = [
      '#type' => 'vertical_tabs',
    ];
    $form['invitation_anonymous'] = [
      '#type' => 'details',
      '#title' => $this->t('Invitation e-mail for anonymous users'),
      '#group' => 'invitation_config_tabs',
      '#open' => TRUE,
    ];
    $form['invitation_anonymous']['invitation_anonymous_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => self::defaultConfiguration()['invitation_anonymous_subject'],
      '#maxlength' => 180,
    ];
    $form['invitation_anonymous']['invitation_anonymous_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => self::defaultConfiguration()['invitation_anonymous_body'],
      '#rows' => 15,
    ];
    $form['existing_user_invitation'] = [
      '#type' => 'details',
      '#title' => $this->t('Invitation e-mail for registered users'),
      '#group' => 'invitation_config_tabs',
    ];
    $form['existing_user_invitation']['existing_user_invitation_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => self::defaultConfiguration()['existing_user_invitation_subject'],
      '#maxlength' => 180,
    ];
    $form['existing_user_invitation']['existing_user_invitation_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => self::defaultConfiguration()['existing_user_invitation_body'],
      '#rows' => 15,
    ];
    $form['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [
        'private_message_invite',
        'private_message_thread',
        'user',
      ],
    ];
    return parent::buildForm($form, $formState);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $config = \Drupal::service('config.factory')->getEditable('private_message_invite.settings');
    $config->set('invitation_anonymous_subject', (string) $formState->getValue('invitation_anonymous_subject'))
      ->set('invitation_anonymous_body', (string) $formState->getValue('invitation_anonymous_body'))
      ->set('existing_user_invitation_subject', (string) $formState->getValue('existing_user_invitation_subject'))
      ->set('existing_user_invitation_body', (string) $formState->getValue('existing_user_invitation_body'))
      ->save();
    parent::submitForm($form, $formState);
  }

}
