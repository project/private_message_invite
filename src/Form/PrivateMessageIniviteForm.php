<?php

namespace Drupal\private_message_invite\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\private_message\Entity\PrivateMessageThread;
use Drupal\private_message_invite\Entity\PrivateMessageInviteEntity;

/**
 * Defines the invite form.
 */
class PrivateMessageIniviteForm extends FormBase implements ContainerInjectionInterface {

  const PENDING = 0;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bulk_group_invitation';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['email_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Invite Email'),
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    $form['actions']['submit_cancel'] = [
      '#type' => 'submit',
      '#weight' => 999,
      '#value' => $this->t('Back to Thread'),
      '#submit' => [[$this, 'cancelForm']],
      '#limit_validation_errors' => [],
    ];
    return $form;
  }

  /**
   * Get Thread Entity.
   */
  private function getEntity() {
    $thread = \Drupal::routeMatch()->getParameter('private_message_thread');
    if (is_object($thread)) {
      $entity = $thread;
    }
    else {
      $entity = PrivateMessageThread::load($thread);
    }
    return $entity;
  }

  /**
   * Cancel form taking you back to a group.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $form_state->setRedirect('entity.private_message_thread.canonical', [
      'private_message_thread' => $entity->id(),
      [],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $entity = $this->getEntity();
    $this->validateEmails($form_state, $values['email_address']);
    $this->validateExistingMembers($form_state, $values['email_address'], $entity);
    $this->validateInviteDuplication($form_state, $values['email_address'], $entity);
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $current_user_id = \Drupal::currentUser()->id();
    $entity = $this->getEntity();
    $private_message_invite = PrivateMessageInviteEntity::create();
    $private_message_invite->set('invite_email', $values['email_address']);
    $private_message_invite->set('invite_status', self::PENDING);
    $private_message_invite->set('created_by', $current_user_id);
    $private_message_invite->set('pm_thread', $entity->id());
    $private_message_invite->save();
  }

  /**
   * Validate emails, display error message if not valid.
   */
  private function validateEmails(FormStateInterface $form_state, $email) {
    $invalid_email = '';
    if (!\Drupal::service('email.validator')->isValid($email)) {
      $invalid_email = $email;
    }

    if (!empty($invalid_email)) {
      $message_singular = "The @error_message is not a valid e-mail address.";
      $this->displayErrorMessage($invalid_email, $message_singular, $form_state);
    }
  }

  /**
   * Validate Existing Members.
   */
  private function validateExistingMembers(FormStateInterface $form_state, $email, $entity) {
    $invalid_email = '';
    $values = $form_state->getValues();
    if ($user = user_load_by_mail($email)) {
      $membership = $entity->isMember($user->id());
      if (!empty($membership)) {
        $invalid_email = $email;
      }
    }

    if (!empty($invalid_email)) {
      $message_singular = "User with @error_message e-mail already a member of this Thread.";
      $this->displayErrorMessage($invalid_email, $message_singular, $form_state);
    }
  }

  /**
   * Validate Invite Duplication for Members.
   */
  private function validateInviteDuplication(FormStateInterface $form_state, $email, $entity) {
    $invalid_email = '';
    $ids = \Drupal::entityQuery('private_message_invite')->condition('invite_email', $email)->condition('pm_thread', $entity->id())->execute();
    if (!empty($ids)) {
      $invalid_email = $email;
    }

    if (!empty($invalid_email)) {
      $message_singular = "Invitation to @error_message already sent.";
      $this->displayErrorMessage($invalid_email, $message_singular, $form_state);
    }
  }

  /**
   * Display Errors.
   */
  private function displayErrorMessage($invalid_email, $message_singular, FormStateInterface $form_state, $message_plural = NULL, $count = 1) {
    $error_message = $invalid_email;
    $form_state->setErrorByName('email_address',
      $this->formatPlural(
        $count,
        $message_singular,
        $message_plural,
        [
          '@error_message' => $error_message,
        ]
      )
    );
  }

}
