<?php

namespace Drupal\private_message_invite\EventSubscriber;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\private_message_invite\Entity\PrivateMessageInviteEntity;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Private message_invite module event subscriber.
 *
 * @package Drupal\private_message_invite\EventSubscriber
 */
class PrivateMessageinviteSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The current user's account object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs private_message_inviteSubscriber.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   */
  public function __construct(AccountInterface $current_user, MessengerInterface $messenger, LoggerChannelFactoryInterface $logger_factory) {
    $this->currentUser = $current_user;
    $this->messenger = $messenger;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * Notify user about Pending invitations.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The RequestEvent to process.
   */
  public function notifyAboutPendingInvitations(RequestEvent $event) {
    $user_id = $this->currentUser->id();
    $properties = [
      'invite_uid' => $user_id,
      'invite_status' => PrivateMessageInviteEntity::PENDING,
    ];

    $invitations = \Drupal::entityTypeManager()->getStorage('private_message_invite')->loadByProperties($properties);
    if (isset($invitations) && !empty($invitations)) {
      // Exclude routes where this info is redundant or will generate a
      // misleading extra message on the next request.
      $route_exclusions = [
        'view.my_pm_thread_invites.page_1',
        'private_message_invite.accept',
        'private_message_invite.decline',
      ];

      $route = $event->getRequest()->get('_route');
      if (!empty($route) && !in_array($route, $route_exclusions)) {
        $messenger = \Drupal::messenger();
        $destination = Url::fromRoute('view.my_pm_thread_invites.page_1', ['user' => $user_id])->toString();
        $replace = ['@url' => $destination];
        $message = $this->t('You have pending Message Thread invitation. <a href="@url">Visit your profile</a> to see them.', $replace);
        $messenger->addMessage($message, 'warning', FALSE);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['notifyAboutPendingInvitations'];
    return $events;
  }

}
