<?php

namespace Drupal\private_message_invite\Controller;

use Drupal\private_message_invite\Entity\PrivateMessageInviteEntity;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;

/**
 * Private message_invite module Controller.
 *
 * @package Drupal\private_message_invite\Controller
 */
class PrivateMessageInviteController extends ControllerBase {

  /**
   * Invite Email Configuration form Rendering.
   */
  public function configPage() {
    return [
      'form' => \Drupal::formBuilder()->getForm('Drupal\private_message_invite\Form\ConfigForm'),
    ];
  }

  /**
   * Invite Email Accept.
   */
  public function accept(Request $request, PrivateMessageInviteEntity $private_message_invite) {
    $invited = $private_message_invite->get('invite_uid')->entity;
    $private_message_invite->set('invite_status', PrivateMessageInviteEntity::ACCEPT);
    $private_message_invite->save();
    $message_thread = $private_message_invite->get('pm_thread')->entity;
    $members = $message_thread->get('members')->getValue();
    $members[count($members)]['target_id'] = $invited->id();
    $message_thread->set('members', $members);
    $message_thread->save();
    \Drupal::messenger()->addStatus($this->t('You have accepted the invitation.'));
    $url = Url::fromRoute('entity.private_message_thread.canonical', ['private_message_thread' => $message_thread->id()], ['absolute' => TRUE]);
    $response = new RedirectResponse($url->toString());
    return $response->send();
  }

  /**
   * Invite Email Decline.
   */
  public function decline(Request $request, PrivateMessageInviteEntity $private_message_invite) {
    $currentUser = \Drupal::currentUser();
    $private_message_invite->delete();
    $created_by = $private_message_invite->get('created_by')->entity;
    $entity = $private_message_invite->get('pm_thread')->entity;
    $url = Url::fromRoute('user.page', [], ['absolute' => 'true']);
    $message = $this->t('You have Rejected the invitation.');
    if ($created_by->id() == $currentUser->id()) {
      $message = $this->t('You have Removed the invitation.');
      $url = Url::fromRoute('view.my_pm_thread_invites.page_2', ['private_message_thread' => $entity->id()], ['absolute' => TRUE]);
    }
    \Drupal::messenger()->addStatus($this->t('You have Rejected the invitation.'));
    $response = new RedirectResponse($url->toString());
    return $response->send();
  }

  /**
   * Invite Email Access check.
   */
  public function checkAccess(PrivateMessageInviteEntity $private_message_invite) {
    $currentUser = \Drupal::currentUser();
    $invited = $private_message_invite->get('invite_uid')->entity;
    $created_by = $private_message_invite->get('created_by')->entity;
    $entity = $private_message_invite->get('pm_thread')->entity;
    $membership = $entity->isMember($currentUser->id());
    // Only allow user accept/decline own invitations.
    if (isset($invited) && !empty($invited)) {
      if ($invited->id() == $currentUser->id() && !$membership) {
        return AccessResult::allowed();
      }
      elseif ($created_by->id() == $currentUser->id()) {
        return AccessResult::allowed();
      }
    }
    elseif ($created_by->id() == $currentUser->id()) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

}
