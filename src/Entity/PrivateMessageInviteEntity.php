<?php

namespace Drupal\private_message_invite\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Defines the private_message_invite entity.
 *
 * @ingroup private_message_invite
 *
 * @ContentEntityType(
 *   id = "private_message_invite",
 *   label = @Translation("Private Message Invites"),
 *   base_table = "private_message_invite",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "invite_email" = "invite_email",
 *     "invite_status" = "invite_status",
 *     "pm_thread" = "pm_thread",
 *   },
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 * )
 */
class PrivateMessageInviteEntity extends ContentEntityBase {

  const PENDING = 0;

  const ACCEPT = 1;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Private Message Invite entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Private Message Invite entity.'))
      ->setReadOnly(TRUE);

    $fields['invite_email'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Invite Email'))
      ->setDescription(t('The Invite Email of the Private Message Invite entity.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['invite_status'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Invite Status'))
      ->setDescription(t('The Invite Status of the Private Message Invite entity.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['invite_uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Invite uid'))
      ->setSettings([
        'target_type' => 'user',
      ])
      ->setDescription(t('The Invite uid of the Private Message Invite entity.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['created_by'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Created By'))
      ->setSettings([
        'target_type' => 'user',
      ])
      ->setDescription(t('The Created By of the Private Message Invite entity.'))
      ->setDisplayConfigurable('view', TRUE);

    $current_date_time = DrupalDateTime::createFromTimestamp(time())->format('Y-m-d\TH:i:s');

    $fields['created'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Created'))
      ->setDefaultValue($current_date_time)
      ->setDescription(t('The Created of the Private Message Invite entity.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['pm_thread'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Message Thread'))
      ->setSettings([
        'target_type' => 'private_message_thread',
      ])
      ->setDescription(t('The Message Thread of the Private Message Invite entity.'))
      ->setDisplayConfigurable('view', TRUE);
    return $fields;
  }

}
